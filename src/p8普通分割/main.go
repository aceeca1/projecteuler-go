package main

import (
	"fmt"
	"os"
	"runtime"
	"strings"
)

type ProductZ struct {
	product, zero int
}

func (u *ProductZ) add(c byte) {
	if c == '0' {
		u.zero++
	} else {
		u.product *= int(c - '0')
	}
}

func (u *ProductZ) del(c byte) {
	if c == '0' {
		u.zero--
	} else {
		u.product /= int(c - '0')
	}
}

func (u *ProductZ) prod() int {
	if u.zero == 0 {
		return u.product
	} else {
		return 0
	}
}

const R = 20
const N = 13

func maxProduct(b string, ch chan int) {
	var ans = 0
	var pz = ProductZ{1, 0}
	for i := 0; i < N; i++ {
		pz.add(b[i])
	}
	for i := N; ; i++ {
		var prod = pz.prod()
		if ans < prod {
			ans = prod
		}
		if i == len(b) {
			break
		}
		pz.add(b[i])
		pz.del(b[i-N])
	}
	ch <- ans
}

func main() {
	file, _ := os.Open("input.txt")
	var s [R]string
	for i := 0; i < R; i++ {
		fmt.Fscan(file, &s[i])
	}
	file.Close()
	var sc = strings.Join(s[:], "")
	var ng = runtime.NumCPU()
	var ch = make(chan int)
	var p1 = 0
	for i := 0; i < ng; i++ {
		var p2 = len(sc) * (i + 1) / ng
		if i != ng-1 {
			go maxProduct(sc[p1:p2+(N-1)], ch)
		} else {
			go maxProduct(sc[p1:p2], ch)
		}
	}
	var ans = 0
	for i := 0; i < ng; i++ {
		var a = <-ch
		if ans < a {
			ans = a
		}
	}
	fmt.Println(ans)
}
