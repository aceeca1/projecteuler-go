package main

import (
	"fmt"
	"os"
	"runtime"
)

const N = 100

type CalcIn struct {
	a1, a2 float64
}

func calculator(in chan CalcIn, out chan float64) {
	for i := range in {
		out <- i.a1 + i.a2
	}
}

func allocator(n int, in chan float64, ans chan float64) {
	if n == 1 {
		ans <- <-in
		return
	}
	var ng = runtime.NumCPU()
	if n>>1 < ng {
		ng = n >> 1
	}
	var preCalc = make(chan CalcIn)
	var postCalc = make(chan float64)
	for i := 0; i < ng; i++ {
		go calculator(preCalc, postCalc)
	}
	for i := 0; i < ng; i++ {
		preCalc <- CalcIn{<-in, <-in}
	}
	for i := ng + ng; i < n; i++ {
		preCalc <- CalcIn{<-in, <-postCalc}
	}
	for i := 1; i < ng; i++ {
		preCalc <- CalcIn{<-postCalc, <-postCalc}
	}
	ans <- <-postCalc
}

func main() {
	var in = make(chan float64)
	var ans = make(chan float64)
	go allocator(N, in, ans)
	file, _ := os.Open("input.txt")
	for i := 0; i < N; i++ {
		var n float64
		fmt.Fscan(file, &n)
		in <- n
	}
	var s = fmt.Sprintf("%.0f", <-ans)
	fmt.Println(s[0:10])
}
