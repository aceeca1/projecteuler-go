package main

import (
	"fmt"
	"runtime"
)

func f(n int) int {
	if n&1 == 0 {
		return n >> 1
	} else {
		return n + n + (n + 1)
	}
}

const N = 1000000

var a = [N + 1]int{}

type AccessorIn struct {
	k, v   int
	sender chan int
}

func accessor(in chan AccessorIn) {
	for i := range in {
		if i.sender == nil {
			a[i.k] = i.v
		} else {
			i.sender <- a[i.k]
		}
	}
}

func hash(k, ng int) uint {
    var uk = uint(k)
    uk *= uint(0x9ddfea08eb382d69)
    uk ^= uk >> 47
    return uk % uint(ng)
}

func calculator(
    ng int,
    ch chan int,
    access []chan AccessorIn,
    done chan bool,
) {
	for i := range ch {
		var re = make(chan int)
		var len = 0
		var from = 0
		for k := i; ; len, k = len+1, f(k) {
			if k <= N {
				access[hash(k, ng)] <- AccessorIn{k, 0, re}
				var r = <-re
				if r != 0 {
					len += r
					from = k
					break
				}
			}
		}
		for k := i; k != from; len, k = len-1, f(k) {
			if k <= N {
				access[hash(k, ng)] <- AccessorIn{k, len, nil}
			}
		}
	}
    done <- true
}

func main() {
	a[1] = 1
	var ng = runtime.NumCPU()
	var access = make([]chan AccessorIn, ng)
    for i := 0; i < ng; i++ {
        access[i] = make(chan AccessorIn)
        go accessor(access[i])
    }
	var ch = make(chan int)
    var done = make(chan bool)
	for i := 0; i < ng; i++ {
		go calculator(ng, ch, access, done)
	}
	for i := 1; i <= N; i++ {
		ch <- i
	}
    close(ch)
    for i := 0; i < ng; i++ {
        <-done
    }
    for i := 0; i < ng; i++ {
        close(access[i])
    }
	var ans = 0
	var arg = 0
	for i := 1; i <= N; i++ {
		if ans < a[i] {
			ans = a[i]
			arg = i
		}
	}
	fmt.Println(arg)
}
