package main

import (
	"fmt"
	"os"
)

const N = 20

var DX = [...]int{0, 1, 1, 1}
var DY = [...]int{1, 0, 1, -1}

var a [N][N]int

func product(x, y, d int) int {
	var ans = 1
	for i := 0; i < 4; i++ {
		if !(0 <= x && x < N) {
			return 0
		}
		if !(0 <= y && y < N) {
			return 0
		}
		ans *= a[x][y]
		x += DX[d]
		y += DY[d]
	}
	return ans
}

func line(i1 int, ch chan int) {
	var ans = 0
	for i2 := 0; i2 < N; i2++ {
		for j := 0; j < len(DX); j++ {
			var u = product(i1, i2, j)
			if ans < u {
				ans = u
			}
		}
	}
	ch <- ans
}

func main() {
	file, _ := os.Open("input.txt")
	for i := 0; i < N; i++ {
		for j := 0; j < N; j++ {
			fmt.Fscan(file, &a[i][j])
		}
	}
	var ch = make(chan int)
	for i := 0; i < N; i++ {
		go line(i, ch)
	}
	var ans = 0
	for i := 0; i < N; i++ {
		var u = <-ch
		if ans < u {
			ans = u
		}
	}
	fmt.Println(ans)
}
