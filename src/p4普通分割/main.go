package main

import (
	"fmt"
	"strconv"
)

func isPalindrome(s string) bool {
	for i1, i2 := 0, len(s)-1; i1 < i2; i1, i2 = i1+1, i2-1 {
		if s[i1] != s[i2] {
			return false
		}
	}
	return true
}

func f(a1 int, ch chan int) {
	var ans = 0
	for a2 := 100; a2 < 1000; a2++ {
		var a = a1 * a2
		if isPalindrome(strconv.Itoa(a)) && ans < a {
			ans = a
		}
	}
	ch <- ans
}

func main() {
	var ch = make(chan int)
	for a1 := 100; a1 < 1000; a1++ {
		go f(a1, ch)
	}
	var ans = 0
	for a1 := 100; a1 < 1000; a1++ {
		var a = <-ch
		if ans < a {
			ans = a
		}
	}
	fmt.Println(ans)
}
