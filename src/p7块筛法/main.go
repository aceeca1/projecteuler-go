package main

import (
	"fmt"
	"math"
)

const N = 10001

func sieveSegmented(u, v []bool, start int, done chan bool) {
	for i := 2; i < len(u); i++ {
		if !u[i] {
			var lb = i * i
			var lb1 = (start + i - 1) / i * i
			if lb < lb1 {
				lb = lb1
			}
			var ub = len(v) + start
			for j := lb; j < ub; j += i {
				v[j-start] = true
			}
		}
	}
	done <- true
}

func findNth(a [][]bool) int {
	var n = 0
	var k = 0
	for i := 0; i < len(a); i++ {
		for j := 0; j < len(a[i]); j++ {
			if !a[i][j] && n >= 2 {
				k++
				if k == N {
					return n
				}
			}
			n++
		}
	}
	return -1
}

func main() {
	var m = int(N * math.Log(2.0*N*math.Log(N)))
	var k = int(math.Sqrt(float64(m))) + 1
	var a = make([][]bool, k)
	for i := 0; i < k; i++ {
		a[i] = make([]bool, k)
	}
	var done = make(chan bool)
	go sieveSegmented(a[0], a[0], 0, done)
	<-done
	for i := 1; i < k; i++ {
		go sieveSegmented(a[0], a[i], i*k, done)
	}
	for i := 1; i < k; i++ {
		<-done
	}
	fmt.Println(findNth(a))
}
