package main

import (
	"fmt"
	"runtime"
)

const N = 1000

func main() {
	var ng = runtime.NumCPU()
	var ch = make(chan int)
	var s = 1
	for i := 0; i < ng; i++ {
		var t = N * (i + 1) / ng
		go func(s, t int) {
			var sum int
			for i := s; i < t; i++ {
				if i%3 == 0 || i%5 == 0 {
					sum += i
				}
			}
			ch <- sum
		}(s, t)
		s = t
	}
	var sum = 0
	for i := 0; i < ng; i++ {
		sum += <-ch
	}
	fmt.Println(sum)
}
