package main

import (
	"fmt"
	"runtime"
)

const N = 600851475143
const B = 1024

type CalcIn struct {
	n, k int
}

type CalcOut struct {
	n int
	a []int
}

type Calculator struct {
	in  chan CalcIn
	out chan CalcOut
}

func (c Calculator) start() {
	for i := range c.in {
		var a = []int{}
		for j := i.k; j < i.k+B; j++ {
			for i.n%j == 0 {
				i.n /= j
				a = append(a, j)
			}
		}
		c.out <- CalcOut{i.n, a}
	}
}

func main() {
	var ng = runtime.NumCPU()
	var calculator = make([]Calculator, ng)
	for i := 0; i < ng; i++ {
		calculator[i].in = make(chan CalcIn)
		calculator[i].out = make(chan CalcOut)
		go calculator[i].start()
	}
	var n = N
	var a = []int{}
	var i = 2
	for i*i <= n {
		var k = i
		for j := 0; j < ng; j++ {
			calculator[j].in <- CalcIn{n, k}
			k += B
		}
		var valid = true
		k, i = i, k
		for j := 0; j < ng; j++ {
			k += B
			var out = <-calculator[j].out
			if valid {
				a = append(a, out.a...)
				if n != out.n {
					n = out.n
					i = k
					valid = false
				}
			}
		}
	}
	if 1 < n {
		a = append(a, n)
	}
	for i := 0; i < ng; i++ {
		close(calculator[i].in)
	}
	fmt.Println(a[len(a)-1])
}
