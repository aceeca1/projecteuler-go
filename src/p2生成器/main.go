package main

import (
	"fmt"
)

const N = 4000000

func fibonacci(a1, a2 int, req chan bool, ch chan int) {
	for <-req {
		ch <- a1
		a1, a2 = a2, a1+a2
	}
}

func main() {
	var req = make(chan bool)
	var ch = make(chan int)
	go fibonacci(1, 1, req, ch)
	var sum int
	req <- true
	for i := range ch {
		if 4000000 < i {
			req <- false
			break
		}
		if (i & 1) == 0 {
			sum += i
		}
		req <- true
	}
	fmt.Println(sum)
}
