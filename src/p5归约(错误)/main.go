package main

import (
	"fmt"
	"runtime"
)

const N = 20

func gcd(a1, a2 int) int {
	for a2 != 0 {
		var a3 = a1 % a2
		a1 = a2
		a2 = a3
	}
	return a1
}

type CalcIn struct {
	a1, a2 int
}

func calculator(in chan CalcIn, out chan int) {
	for i := range in {
		out <- i.a1 / gcd(i.a1, i.a2) * i.a2
	}
}

func allocator(n int, in chan int, out chan CalcIn, ans chan int) {
	for i := 1; i < n; i++ {
		out <- CalcIn{<-in, <-in}
	}
	close(out)
	ans <- <-in
}

func main() {
	var ng = runtime.NumCPU()
	var preCalc = make(chan CalcIn)
	var postCalc = make(chan int)
	var ans = make(chan int)
	go allocator(N, postCalc, preCalc, ans)
	for i := 0; i < ng; i++ {
		go calculator(preCalc, postCalc)
	}
	for i := 1; i <= N; i++ {
		postCalc <- i
	}
	fmt.Println(<-ans)
}
