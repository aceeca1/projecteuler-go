package main

import (
	"fmt"
)

const N = 100

func main() {
	var a1 = N * (N + 1) >> 1
	var a2 = N * (N + 1) * (N + N + 1) / 6
	fmt.Println(a1*a1 - a2)
}
