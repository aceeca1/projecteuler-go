package main

import (
	"fmt"
	"runtime"
)

func found(n int) bool {
	var i = 1
	var divisors = 0
	for ; i*i < n; i++ {
		if n%i == 0 {
			divisors += 2
		}
	}
	if i*i == n {
		divisors++
	}
	return 500 < divisors
}

func calculator(ch chan int, ans chan int) {
	for a := range ch {
		if found(a) {
			ans <- a
		}
	}
}

func main() {
	var ch = make(chan int)
	var ans = make(chan int)
	var ng = runtime.NumCPU()
	for i := 0; i < ng; i++ {
		go calculator(ch, ans)
	}
	var triangle = 1
	for i := 2; ; i++ {
		select {
		case ch <- triangle:
		case u := <-ans:
            close(ch)
			fmt.Println(u)
			return
		}
		triangle += i
	}
}
