package main

import (
	"fmt"
	"runtime"
)

func found(a int) int {
	var s1 = 1000 - a
	var s2 = a * a
	if s2%s1 != 0 {
		return 0
	}
	var b2 = s1 - s2/s1
	if b2 < 0 || (b2&1) != 0 {
		return 0
	}
	var b = b2 >> 1
	var c = s1 - b
	return a * b * c
}

func calculator(ch chan int, ans chan int) {
	for a := range ch {
		var u = found(a)
		if u != 0 {
			ans <- u
		}
	}
}

func main() {
	var ch = make(chan int)
	var ans = make(chan int)
	var ng = runtime.NumCPU()
	for i := 0; i < ng; i++ {
		go calculator(ch, ans)
	}
	for a := 1; a <= 1000; a++ {
		select {
		case ch <- a:
		case u := <-ans:
			fmt.Println(u)
			return
		}
	}
}
