package main

import (
	"fmt"
	"sort"
)

func init() { reg(77, solve77) }

func solve77() {
	var f = func(n int) []int {
		var a = make([]int, n+1)
		a[0] = 1
		for i := NewPrime16(); i.Get() <= n; i.Next() {
			for j := i.Get(); j <= n; j++ {
				a[j] += a[j-i.Get()]
			}
		}
		return a
	}
	var n = 1
	for {
		var a = f(n)
		if a[n] >= 5000 {
			fmt.Println(sort.Search(n, func(i int) bool {
				return a[i] >= 5000
			}))
			return
		}
		n += n
	}
}
