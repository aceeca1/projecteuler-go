package main

import (
	"fmt"
	"strconv"
)

func init() { reg(34, solve34) }

func solve34() {
	var fac = [10]int{1, 1}
	for i, a := 2, 1; i < 10; i++ {
		a *= i
		fac[i] = a
	}
	var r = 0
	for i := fac[9] * 7; i >= 10; i-- {
		var sum = 0
		digitsForeach(strconv.Itoa(i), func(i int) { sum += fac[i] })
		if i == sum {
			r += i
		}
	}
	fmt.Printf("%d\n", r)
}
