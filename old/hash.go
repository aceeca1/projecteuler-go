package main

func hash(n uint32) uint32 {
	const k = uint32(0xeb382d5b)
	var a = uint32(0xdeadbeef)
	a = (a ^ n) * k
	return (a ^ n) * k
}
