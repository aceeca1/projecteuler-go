package main

import (
	"fmt"
	"sort"
)

func init() { reg(22, solve22) }

func solve22() {
	sort.Strings(a022Data[:])
	var sum = int64(0)
	for i, v := range a022Data {
		var u = int64(0)
		for _, j := range v {
			u += int64(j - 64)
		}
		sum += u * int64(i+1)
	}
	fmt.Println(sum)
}
