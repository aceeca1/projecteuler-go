package main

import (
	"fmt"
	"math/big"
)

func init() { reg(75, solve75) }

func solve75() {
	const n = 1500000
	var oneB = big.NewInt(1)
	var m = map[int]int{}
	for i := 1; (1+i)*(1+i+i)<<1 <= n+n; i++ {
		var iB = big.NewInt(int64(i))
		for j, p := i+1, 0; p <= n; j += 2 {
			p = j * (j + i) << 1
			var jB = big.NewInt(int64(j))
			if jB.GCD(nil, nil, jB, iB).Cmp(oneB) == 0 {
				for k := p; k <= n; k += p {
					m[k] = m[k] + 1
				}
			}
		}
	}
	var a = 0
	for _, v := range m {
		if v == 1 {
			a++
		}
	}
	fmt.Println(a)
}
