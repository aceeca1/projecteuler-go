package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

func init() { reg(70, solve70) }

func solve70() {
	const n = 10000000
	var p = NewPrime16().phi(n)
	var m, mI = math.Inf(1), 0
	for i := 2; i <= n; i++ {
		if (p[i]-i)%9 == 0 {
			var bP = []byte(strconv.Itoa(p[i]))
			sort.Sort(ByteSlice(bP))
			var bI = []byte(strconv.Itoa(i))
			sort.Sort(ByteSlice(bI))
			if string(bI) == string(bP) {
				var r = float64(i) / float64(p[i])
				if r < m {
					m, mI = r, i
				}
			}
		}
	}
	fmt.Println(mI)
}
