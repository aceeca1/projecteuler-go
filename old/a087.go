package main

import (
	"fmt"
)

func init() { reg(87, solve87) }

func solve87() {
	var n, p = 50000000, NewPrime16()
	var r = [3][]int{}
	for i, f := range []func(int) int{
		func(n int) int { return n * n },
		func(n int) int { return n * n * n },
		func(n int) int { return n * n * (n * n) },
	} {
		for j := p.Iter(); ; j.Next() {
			var k = f(j.Get())
			if k >= n {
				break
			}
			r[i] = append(r[i], k)
		}
	}
	var m = map[int]bool{}
	for _, i := range r[0] {
		for _, j := range r[1] {
			for _, k := range r[2] {
				var v = i + j + k
				if v < n {
					m[v] = true
				}
			}
		}
	}
	fmt.Println(len(m))
}
