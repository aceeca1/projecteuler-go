package main

import (
	"fmt"
	"sort"
	"strconv"
)

func init() { reg(49, solve49) }

func solve49() {
	var p = NewPrime16()
	for p.Get() < 1000 {
		p.Next()
	}
	var b = map[string][]int{}
	for ; p.Get() < 10000; p.Next() {
		var a = []byte(strconv.Itoa(p.Get()))
		sort.Sort(ByteSlice(a))
		var e = string(a)
		b[e] = append(b[e], p.Get())
	}
	for _, i := range b {
		sort.Ints(i)
		for j0 := 0; j0 < len(i); j0++ {
			for j1 := j0 + 1; j1 < len(i); j1++ {
				for j2 := j1 + 1; j2 < len(i); j2++ {
					if i[j0]+i[j2] == i[j1]+i[j1] && i[j0] != 1487 {
						fmt.Print(i[j0])
						fmt.Print(i[j1])
						fmt.Println(i[j2])
						return
					}
				}
			}
		}
	}
}
