package main

type Eq interface {
	Equal(n Eq) bool
}

type IntEq int

func (n1 IntEq) Equal(n2 Eq) bool {
	return n1 == n2.(IntEq)
}

type Int3Eq [3]int

func (n1 Int3Eq) Equal(n2 Eq) bool {
	return n1 == n2.(Int3Eq)
}

func findCycle(n Eq, f func(*Eq)) int {
	var n1, n2 = 0, 0
	var x1, x2 = n, n
	f(&x2)
	for !x2.Equal(x1) {
		if n2 > n1+n1 {
			n1 = n2
			x1 = x2
		}
		n2++
		f(&x2)
	}
	return n2 - n1
}
