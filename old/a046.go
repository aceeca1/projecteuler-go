package main

import (
	"container/heap"
	"fmt"
)

func init() { reg(46, solve46) }

type a046Iter struct{ p, q, r int }
type a046Heap struct{ AnyS }
type a046HeapEtc struct {
	a *a046Heap
	p *Prime16
}

func NewA046Iter(p int) *a046Iter { return &a046Iter{p, 0, p} }
func (m *a046Iter) Get() int      { return m.r }
func (m *a046Iter) Next()         { m.q++; m.r = m.p + (m.q * m.q << 1) }

func (m *a046Heap) Less(i, j int) bool {
	var mi, mj = (*m.AnyS[i]).(a046Iter), (*m.AnyS[j]).(a046Iter)
	return mi.Get() < mj.Get()
}

func NewA046HeapEtc() (m *a046HeapEtc) {
	m = &a046HeapEtc{new(a046Heap), NewPrime16()}
	heap.Push(m.a, *NewA046Iter(m.p.Get()))
	return
}
func (m *a046HeapEtc) Get() int {
	var m0 = (*m.a.AnyS[0]).(a046Iter)
	return m0.Get()
}
func (m *a046HeapEtc) Next() {
	var i = heapPop(m.a).(a046Iter)
	i.Next()
	if i.p == m.p.Get() {
		m.p.Next()
		heap.Push(m.a, *NewA046Iter(m.p.Get()))
	}
	heap.Push(m.a, i)
}

func solve46() {
	var m = 3
	for i := NewA046HeapEtc(); ; i.Next() {
		if i.Get() == m {
			m += 2
		} else if i.Get() > m {
			fmt.Println(m)
			return
		}
	}
}
