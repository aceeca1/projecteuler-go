package main

import (
	"fmt"
)

func init() { reg(39, solve39) }

func solve39() {
	var w = [1001]map[uint32]bool{}
	for i := 0; i <= 1000; i++ {
		w[i] = map[uint32]bool{}
	}
	for k := 1; k <= 500; k++ {
		for m := 1; k*m*m < 500; m++ {
			for n := 1; n < m && k*m*(m+n) <= 500; n++ {
				var m2, n2 = m * m, n * n
				var a = uint32(k * (m2 - n2))
				var b = uint32(k * m * n << 1)
				var c = uint32(k * (m2 + n2))
				w[a+b+c][hash(a)^hash(b)^hash(c)] = true
			}
		}
	}
	var m, mI = 0, 0
	for i, v := range w {
		if len(v) > m {
			m, mI = len(v), i
		}
	}
	fmt.Println(mI)
}
