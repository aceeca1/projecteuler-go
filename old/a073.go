package main

import (
	"fmt"
)

func init() { reg(73, solve73) }

func solve73() {
	var p = NewPrime16()
	var s = 0
	for i := 1; i <= 12000; i++ {
		var m = map[int]bool{}
		for _, j := range p.Factorize32(i) {
			m[j] = true
		}
		var q = make([]int, 0, len(m))
		for j := range m {
			q = append(q, j)
		}
		var f func(int, []int) int
		f = func(n int, k []int) int {
			if len(k) == 0 {
				return n
			}
			return f(n, k[1:]) - f(n/k[0], k[1:])
		}
		s += f((i-1)>>1, q) - f(i/3, q)
	}
	fmt.Println(s)
}
