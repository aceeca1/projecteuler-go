package main

import (
	"math"
)

func polyNum(s, n int64) int64 { return (n*(n-1)>>1)*(s-2) + n }

func isPolyNum(s, a int64) bool {
	var aF = float64(a)
	switch s {
	case 3:
		return a == polyNum(3, int64(math.Sqrt(aF+aF)))
	case 4:
		return a == polyNum(4, int64(math.Sqrt(aF)))
	}
	return a == polyNum(s, int64(math.Sqrt((aF+aF)/float64(s-2)))+1)
}
