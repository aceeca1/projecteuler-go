package main

import (
	"fmt"
	"strings"
)

func init() { reg(54, solve54) }

func solve54() {
	var in = strings.NewReader(a054Data)
	var read = func() (a [5]string) {
		for i := range a {
			fmt.Fscan(in, &a[i])
		}
		return
	}
	var m = 0
	for i := 0; i < 1000; i++ {
		if pokerHand(read()) > pokerHand(read()) {
			m++
		}
	}
	fmt.Println(m)
}
