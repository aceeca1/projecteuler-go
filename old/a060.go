package main

import (
	"fmt"
)

func init() { reg(60, solve60) }

func a060F(p *Prime16, a []int, s int, b *int) (r bool) {
	for ; ; p.Next() {
		var ae = append(a, p.Get())
		var se = s + p.Get()
		if se+(5-len(ae))*(p.Get()+6-len(ae)) > *b {
			break
		}
		var ok = true
		for _, v := range a {
			if !p.Prime32(intCat(v, p.Get())) ||
				!p.Prime32(intCat(p.Get(), v)) {
				ok = false
				break
			}
		}
		if !ok {
			continue
		}
		if len(ae) == 5 {
			*b = se
			return true
		}
		if a060F(&Prime16{p.p, p.i}, ae, se, b) {
			r = true
		}
	}
	return
}

func solve60() {
	var b = 16
	for !a060F(NewPrime16(), []int{}, 0, &b) {
		b <<= 1
	}
	fmt.Println(b)
}
