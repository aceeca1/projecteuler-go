package main

import (
	"math/big"
	"math/rand"
)

var rnd = rand.New(rand.NewSource(0))

func someDivisor(n *big.Int) *big.Int {
	if n.Int64()&1 == 0 {
		return big.NewInt(2)
	}
	if new(big.Int).Mod(n, big.NewInt(3)).BitLen() == 0 {
		return big.NewInt(3)
	}
	if new(big.Int).Mod(n, big.NewInt(5)).BitLen() == 0 {
		return big.NewInt(5)
	}
	var h = new(big.Int).Set(n)
	for h.Cmp(n) == 0 {
		var c = new(big.Int).Rand(rnd, n)
		c.Add(c, big.NewInt(1))
		var r = new(big.Int).Rand(rnd, n)
		var f = func(x *big.Int) *big.Int {
			x.Mul(x, x)
			x.Add(x, c)
			return x.Mod(x, n)
		}
		var n1, n2 int
		var x1 = new(big.Int).Set(r)
		var x2 = f(r)
		h = big.NewInt(1)
		for h.Cmp(big.NewInt(1)) == 0 {
			if n2 > n1+n1 {
				n1 = n2
				x1.Set(x2)
			}
			n2++
			f(x2)
			var sub = new(big.Int).Sub(x1, x2)
			h.GCD(nil, nil, n, sub.Abs(sub).Add(sub, n))
		}
	}
	return h
}

// This function will consume a.
func Factorize(a *big.Int) []*big.Int {
	if a.Cmp(big.NewInt(1)) == 0 {
		return nil
	}
	if a.ProbablyPrime(7) {
		return []*big.Int{a}
	}
	var b = someDivisor(a)
	return append(Factorize(a.Div(a, b)), Factorize(b)...)
}
