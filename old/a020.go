package main

import (
	"fmt"
	"math/big"
)

func init() { reg(20, solve20) }

func solve20() {
	var a = big.NewInt(2)
	for i := int64(3); i <= 100; i++ {
		a.Mul(a, big.NewInt(i))
	}
	var m = 0
	digitsForeach(a.String(), func(i int) { m += i })
	fmt.Println(m)
}
