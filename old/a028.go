package main

import (
	"fmt"
)

func init() { reg(28, solve28) }

func solve28() {
	var s, a = int64(1), int64(1)
	for i := 2; i <= 1000; i += 2 {
		for j := 4; j != 0; j-- {
			a += int64(i)
			s += a
		}
	}
	fmt.Println(s)
}
