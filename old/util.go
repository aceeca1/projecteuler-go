package main

import (
	"container/heap"
	"math"
	"sort"
)

type Slice heap.Interface

type IntSlice []int
type ByteSlice []byte
type Int64Slice []int64
type StringSlice []string
type Float64Slice []float64
type IntSliceR []int
type ByteSliceR []byte
type Int64SliceR []int64
type StringSliceR []string
type Float64SliceR []float64
type AnyS []*interface{}

func (p IntSlice) Len() int      { return len(p) }
func (p ByteSlice) Len() int     { return len(p) }
func (p Int64Slice) Len() int    { return len(p) }
func (p StringSlice) Len() int   { return len(p) }
func (p Float64Slice) Len() int  { return len(p) }
func (p IntSliceR) Len() int     { return len(p) }
func (p ByteSliceR) Len() int    { return len(p) }
func (p Int64SliceR) Len() int   { return len(p) }
func (p StringSliceR) Len() int  { return len(p) }
func (p Float64SliceR) Len() int { return len(p) }
func (p AnyS) Len() int          { return len(p) }

func (p IntSlice) Less(i, j int) bool      { return p[i] < p[j] }
func (p ByteSlice) Less(i, j int) bool     { return p[i] < p[j] }
func (p Int64Slice) Less(i, j int) bool    { return p[i] < p[j] }
func (p StringSlice) Less(i, j int) bool   { return p[i] < p[j] }
func (p Float64Slice) Less(i, j int) bool  { return p[i] < p[j] }
func (p IntSliceR) Less(i, j int) bool     { return p[i] > p[j] }
func (p ByteSliceR) Less(i, j int) bool    { return p[i] > p[j] }
func (p Int64SliceR) Less(i, j int) bool   { return p[i] > p[j] }
func (p StringSliceR) Less(i, j int) bool  { return p[i] > p[j] }
func (p Float64SliceR) Less(i, j int) bool { return p[i] > p[j] }

func (p IntSlice) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p ByteSlice) Swap(i, j int)     { p[i], p[j] = p[j], p[i] }
func (p Int64Slice) Swap(i, j int)    { p[i], p[j] = p[j], p[i] }
func (p StringSlice) Swap(i, j int)   { p[i], p[j] = p[j], p[i] }
func (p Float64Slice) Swap(i, j int)  { p[i], p[j] = p[j], p[i] }
func (p IntSliceR) Swap(i, j int)     { p[i], p[j] = p[j], p[i] }
func (p ByteSliceR) Swap(i, j int)    { p[i], p[j] = p[j], p[i] }
func (p Int64SliceR) Swap(i, j int)   { p[i], p[j] = p[j], p[i] }
func (p StringSliceR) Swap(i, j int)  { p[i], p[j] = p[j], p[i] }
func (p Float64SliceR) Swap(i, j int) { p[i], p[j] = p[j], p[i] }
func (p AnyS) Swap(i, j int)          { p[i], p[j] = p[j], p[i] }

func (p *IntSlice) Push(n interface{})      { *p = append(*p, n.(int)) }
func (p *ByteSlice) Push(n interface{})     { *p = append(*p, n.(byte)) }
func (p *Int64Slice) Push(n interface{})    { *p = append(*p, n.(int64)) }
func (p *StringSlice) Push(n interface{})   { *p = append(*p, n.(string)) }
func (p *Float64Slice) Push(n interface{})  { *p = append(*p, n.(float64)) }
func (p *IntSliceR) Push(n interface{})     { *p = append(*p, n.(int)) }
func (p *ByteSliceR) Push(n interface{})    { *p = append(*p, n.(byte)) }
func (p *Int64SliceR) Push(n interface{})   { *p = append(*p, n.(int64)) }
func (p *StringSliceR) Push(n interface{})  { *p = append(*p, n.(string)) }
func (p *Float64SliceR) Push(n interface{}) { *p = append(*p, n.(float64)) }
func (p *AnyS) Push(n interface{})          { *p = append(*p, &n) }

func (p *IntSlice) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *ByteSlice) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *Int64Slice) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *StringSlice) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *Float64Slice) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *IntSliceR) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *ByteSliceR) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *Int64SliceR) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *StringSliceR) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *Float64SliceR) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}
func (p *AnyS) Pop() (r interface{}) {
	*p, r = (*p)[:len(*p)-1], (*p)[len(*p)-1]
	return
}

func heapPop(p Slice) interface{} {
	return *heap.Pop(p).(*interface{})
}

type sortT struct {
	length int
	less   func(int, int) bool
	swap   func(int, int)
}

func (c *sortT) Len() int           { return c.length }
func (c *sortT) Less(i, j int) bool { return c.less(i, j) }
func (c *sortT) Swap(i, j int)      { c.swap(i, j) }

func sortF(length int, less func(int, int) bool, swap func(int, int)) {
	sort.Sort(&sortT{length: length, less: less, swap: swap})
}

func reverse(a Slice) {
	for p, q := 0, a.Len()-1; p < q; {
		a.Swap(p, q)
		p++
		q--
	}
}

func nextPerm(a []int) bool {
	if len(a) <= 1 {
		return false
	}
	var p = len(a) - 2
	for p >= 0 && a[p] > a[p+1] {
		p--
	}
	if p == -1 {
		return false
	}
	var q, m = 0, int(^uint(0) >> 1)
	for i, v := range a[p+1:] {
		if v > a[p] && v < m {
			m = v
			q = i
		}
	}
	q += p + 1
	a[p], a[q] = a[q], a[p]
	var v = IntSlice(a[p+1:])
	reverse(&v)
	return true
}

func intLen(i int) int {
	return int(math.Log10(float64(i))) + 1
}

func intCat(i, j int) int {
	return int(math.Pow10(intLen(j)))*i + j
}

func digitsForeach(a string, f func(int)) {
	for _, i := range []byte(a) {
		f(int(i - '0'))
	}
}

func isSquare(x int) bool {
	var a = int(math.Sqrt(float64(x)))
	return x == a*a
}
