package main

import (
	"fmt"
)

func init() { reg(50, solve50) }

func solve50() {
	var a = []int{}
	var b = [1000001]bool{}
	for p := NewPrime16(); p.Get() <= 1000000; p.Next() {
		a = append(a, p.Get())
		b[p.Get()] = true
	}
	var m, mI = 0, 0
	for i, k := range a {
		if i*m >= 1000000 {
			break
		}
		for j := i + 1; j < len(a); j++ {
			k += a[j]
			if k >= 1000000 {
				break
			}
			if b[k] && j-i+1 > m {
				m, mI = j-i+1, k
			}
		}
	}
	fmt.Println(mI)
}
