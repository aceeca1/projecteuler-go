package main

import (
	"fmt"
	"strings"
)

func maxPathSum(n int, p string) int {
	var in = strings.NewReader(p)
	var a = [][]int{}
	for i := 0; i < n; i++ {
		a = append(a, []int{})
		for j := 0; j <= i; j++ {
			var r int
			fmt.Fscanf(in, " %d", &r)
			a[i] = append(a[i], r)
		}
	}
	for i := n - 2; i >= 0; i-- {
		for j := 0; j <= i; j++ {
			var v1, v2 = a[i+1][j], a[i+1][j+1]
			if v1 > v2 {
				a[i][j] += v1
			} else {
				a[i][j] += v2
			}
		}
	}
	return a[0][0]
}
