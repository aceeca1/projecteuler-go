package main

import (
	"fmt"
	"math"
)

func init() { reg(63, solve63) }

func solve63() {
	var a = 0
	for i := 1; i <= 9; i++ {
		var iF, p = float64(i), float64(i)
		for j := 1; j == int(math.Log10(p))+1; j++ {
			a = a + 1
			p = p * iF
		}
	}
	fmt.Println(a)
}
