package main

import (
	"fmt"
	"strconv"
)

func init() { reg(40, solve40) }

func solve40() {
	var b = [...]int{1, 10, 100, 1000, 10000, 100000, 1000000}
	var a = map[int]bool{}
	for _, i := range b {
		a[i-1] = true
	}
	var c, m = 0, 1
	for i := 1; c <= 1000000; i++ {
		digitsForeach(strconv.Itoa(i), func(i int) {
			if a[c] {
				m *= i
			}
			c++
		})
	}
	fmt.Println(m)
}
