package main

import (
	"fmt"
)

func init() { reg(86, solve86) }

func solve86() {
	var pS = 0
	for c := 1; ; c++ {
		var sum = 0
		for p := c + c; p >= 2; p-- {
			if isSquare(p*p + c*c) {
				var lB, uB = p - c, p >> 1
				if lB < 1 {
					lB = 1
				}
				sum += uB - lB + 1
			}
		}
		pS += sum
		if pS > 1000000 {
			fmt.Println(c)
			return
		}
	}
}
