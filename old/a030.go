package main

import (
	"fmt"
	"strconv"
)

func init() { reg(30, solve30) }

func solve30() {
	var f = func(n int) int { return n * n * n * n * n }
	var m = 0
	for i := f(9) * 6; i >= 10; i-- {
		var k = 0
		digitsForeach(strconv.Itoa(i), func(i int) { k += f(i) })
		if i == k {
			m += i
		}
	}
	fmt.Println(m)
}
