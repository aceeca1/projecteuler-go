package main

import (
	"fmt"
	"time"
)

func init() { reg(19, solve19) }

func solve19() {
	var s = 0
	for i := 1901; i <= 2000; i++ {
		for j := 1; j <= 12; j++ {
			var m = time.Month(j)
			if time.Date(i, m, 1, 0, 0, 0, 0, time.UTC).Weekday() == 0 {
				s++
			}
		}
	}
	fmt.Println(s)
}
