package main

import (
	"fmt"
)

func init() { reg(10, solve10) }

func solve10() {
	var sum int64
	for i := NewPrime16(); i.Get() <= 2000000; i.Next() {
		sum += int64(i.Get())
	}
	fmt.Println(sum)
}
