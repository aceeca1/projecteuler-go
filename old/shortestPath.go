package main

import (
	"math"
)

type sPEdge struct {
	t, c int
}

type sPVertex struct {
	e []sPEdge
	d int
	q bool
}

type sPGraph []sPVertex

func (g *sPGraph) addVertex() int {
	*g = append(*g, sPVertex{[]sPEdge{}, math.MaxInt32, false})
	return len(*g) - 1
}

func (g sPGraph) addEdge(s, t, c int) {
	g[s].e = append(g[s].e, sPEdge{t, c})
}

func (g sPGraph) shortestPath(s int) {
	var m = 1<<uint(math.Ilogb(float64(len(g)))+3) - 1
	var dQ = make([]int, m+1)
	var p, q = 0, 1
	g[s].q = true
	g[s].d = 0
	dQ[0] = s
	for p != q {
		var dQH = dQ[p]
		g[dQH].q = false
		p = (p + 1) & m
		for _, i := range g[dQH].e {
			if g[i.t].d > i.c+g[dQH].d {
				g[i.t].d = i.c + g[dQH].d
				if !g[i.t].q {
					g[i.t].q = true
					dQ[q] = i.t
					q = (q + 1) & m
				}
			}
		}
	}
}
