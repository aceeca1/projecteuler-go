package main

import (
	"fmt"
)

func init() { reg(82, solve82) }

func solve82() {
	var g = new(sPGraph)
	g.addVertex()
	g.addVertex()
	var b = [len(a081Data)][len(a081Data[0])]int{}
	for i := range b {
		for j := range b[i] {
			b[i][j] = g.addVertex()
			if j != 0 {
				g.addEdge(b[i][j-1], b[i][j], a081Data[i][j])
			}
			if i != 0 {
				g.addEdge(b[i-1][j], b[i][j], a081Data[i][j])
				g.addEdge(b[i][j], b[i-1][j], a081Data[i-1][j])
			}
		}
		g.addEdge(0, b[i][0], a081Data[i][0])
		g.addEdge(b[i][len(b[i])-1], 1, 0)
	}
	g.shortestPath(0)
	fmt.Println((*g)[1].d)
}
