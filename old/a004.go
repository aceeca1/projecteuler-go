package main

import (
	"fmt"
	"strconv"
)

func init() { reg(4, solve4) }

func solve4() {
	var m = 0
	for i := 999; i >= 100; i-- {
		for j := 999; j >= 100; j-- {
			var x = i * j
			if x <= m {
				break
			}
			var s = strconv.Itoa(x)
			if isPalin(s) {
				m = x
			}
		}
	}
	fmt.Println(m)
}
