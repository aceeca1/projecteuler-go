package main

import (
	"fmt"
	"strconv"
)

func init() { reg(36, solve36) }

func solve36() {
	var m = 0
	for i := NewPalin(); i.Get() < 1000000; i.Next() {
		if isPalin(strconv.FormatInt(int64(i.Get()), 2)) {
			m += i.Get()
		}
	}
	fmt.Println(m)
}
