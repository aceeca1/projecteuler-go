package main

import (
	"fmt"
)

func init() { reg(27, solve27) }

func solve27() {
	var p = NewPrime16()
	var m, mI = 0, 0
	for i := p; i.Get() < 1000; i.Next() {
		for j := -1000; j <= 1000; j++ {
			if v := i.Get() + j + 1; v < 2 || !i.Prime16(v) {
				continue
			}
			if v := i.Get() + j + j + 4; v < 2 || !i.Prime16(v) {
				continue
			}
			var k = 3
			for {
				if v := i.Get() + k*(k+j); v < 2 || i.Prime32(v) {
					break
				}
				k++
			}
			if k > m {
				m, mI = k, i.Get()*j
			}
		}
	}
	fmt.Println(mI)
}
