package main

import (
	"sort"
)

var pokerRank = func() (r [256]int) {
	for i, v := range "23456789TJQKA" {
		r[v] = i
	}
	return
}()

func pokerHand(h [5]string) (re string) {
	var isF = true
	for i := 1; i < 5; i++ {
		if h[i][1] != h[0][1] {
			isF = false
			break
		}
	}
	var r = [13]int{}
	for _, i := range h {
		r[pokerRank[i[0]]]++
	}
	var t = []int{}
	for i, v := range r {
		if v > 0 {
			t = append(t, (v<<4)+i)
		}
	}
	sort.Sort(IntSliceR(t))
	var isS = true
	for i := 1; i < len(t); i++ {
		if t[i] != t[i-1]-1 {
			isS = false
			break
		}
	}
	var isS0 = r == [13]int{1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1}
	switch {
	case isF && isS:
		re = "9"
	case isF && isS0:
		re = "9D"
	case t[0]>>4 == 4:
		re = "8"
	case t[0]>>4 == 3 && t[1]>>4 == 2:
		re = "7"
	case isF:
		re = "6"
	case isS:
		re = "5"
	case isS0:
		re = "50"
	case t[0]>>4 == 3:
		re = "4"
	case t[0]>>4 == 2 && t[1]>>4 == 2:
		re = "3"
	case t[0]>>4 == 2:
		re = "2"
	default:
		re = "1"
	}
	var reB = []byte(re)
	for _, i := range t {
		reB = append(reB, byte(48+i))
	}
	return string(reB)
}
