package main

import (
	"fmt"
	"math/big"
)

func init() { reg(16, solve16) }

func solve16() {
	var a = big.NewInt(1)
	for i := 1000; i != 0; i-- {
		a.Add(a, a)
	}
	var m = 0
	digitsForeach(a.String(), func(i int) { m += i })
	fmt.Println(m)
}
