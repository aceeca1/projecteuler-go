package main

import (
	"fmt"
	"unicode"
)

func init() { reg(8, solve8) }

func solve8() {
	var b = [13]int64{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
	var p int64 = 1
	var m int64 = 0
	for i, e, k := 0, 13, 0; i < len(a008Data); i++ {
		if !unicode.IsDigit(rune(a008Data[i])) {
			continue
		}
		p /= b[k]
		b[k] = int64(a008Data[i] - 48)
		if b[k] == 0 {
			b[k] = 1
			e = 13
		}
		p *= b[k]
		if e <= 0 && p > m {
			m = p
		}
		e--
		k = (k + 1) % 13
	}
	fmt.Println(m)
}
