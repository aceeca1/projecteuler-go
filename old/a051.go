package main

import (
	"fmt"
	"strconv"
	"strings"
)

func init() { reg(51, solve51) }

func solve51() {
	for i := NewPrime16(); ; i.Next() {
		var s = strconv.Itoa(i.Get())
		for nj, j := range []byte("012") {
			if strings.IndexByte(s, j) == -1 {
				continue
			}
			var p = [][]byte{[]byte{}}
			for _, k := range []byte(s) {
				for l, pl := range p {
					if k == j {
						p = append(p, append(append([]byte{}, pl...), '*'))
					}
					p[l] = append(pl, k)
				}
			}
			for _, k := range p[1:] {
				var u = nj
				for l := byte(j + 1); l <= '9'; l++ {
					var q = append([]byte{}, k...)
					for m := range q {
						if q[m] == '*' {
							q[m] = l
						}
					}
					var r, _ = strconv.Atoi(string(q))
					if r < 2 || !i.Prime32(r) {
						u = u + 1
						if u >= 3 {
							break
						}
					}
				}
				if u < 3 {
					fmt.Println(s)
					return
				}
			}
		}
	}
}
