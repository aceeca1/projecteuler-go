package main

import (
	"fmt"
)

func init() { reg(67, solve67) }

func solve67() {
	fmt.Println(maxPathSum(100, a067Data))
}
