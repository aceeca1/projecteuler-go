package main

import (
	"fmt"
	"math/big"
)

func init() { reg(65, solve65) }

func solve65() {
	var a = append(make([]int, 0, 100), 2)
	for i := 2; ; i += 2 {
		a = append(a, 1, i, 1)
		if len(a) >= 100 {
			a = a[:100]
			break
		}
	}
	var s0, s1 = big.NewInt(1), big.NewInt(0)
	for i := len(a) - 1; i >= 0; i-- {
		var ai = big.NewInt(int64(a[i]))
		s0, s1 = ai.Mul(ai, s0).Add(ai, s1), s0
	}
	var s = 0
	digitsForeach(s0.String(), func(i int) { s = s + i })
	fmt.Println(s)
}
