package main

import (
	"fmt"
	"math/big"
)

func init() { reg(33, solve33) }

func solve33() {
	var a = 1
	var b = 1
	for i1 := 1; i1 < 10; i1++ {
		for i0 := 0; i0 < 10; i0++ {
			var i = i1*10 + i0
			var j0 = i1
			for j1 := 1; j1 < 10; j1++ {
				var j = j1*10 + j0
				if i == j || i*j1 != j*i0 {
					continue
				}
				a *= i
				b *= j
			}
		}
	}
	var c = big.NewInt(int64(a))
	var d = big.NewInt(int64(b))
	d.GCD(nil, nil, d, c)
	c.Div(c, d)
	fmt.Println(c)
}
