package main

import (
	"fmt"
	"math/big"
)

func init() { reg(66, solve66) }

func solve66() {
	var a = big.NewInt(0)
	var k = 0
	for i := 1; i <= 1000; i++ {
		var p, _ = pell(i)
		if p != nil && p.Cmp(a) > 0 {
			a, k = p, i
		}
	}
	fmt.Println(k)
}
