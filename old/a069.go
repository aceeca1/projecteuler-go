package main

import (
	"fmt"
	"math"
)

func init() { reg(69, solve69) }

func solve69() {
	var p = NewPrime16()
	var m, mI = math.Inf(1), 0
	for i := 2; i <= 1000000; i++ {
		var u = map[int]bool{}
		for _, j := range p.Factorize32(i) {
			u[j] = true
		}
		var d = 1.0
		for j := range u {
			d = d * (1.0 - 1.0/float64(j))
		}
		if d < m {
			m, mI = d, i
		}
	}
	fmt.Println(mI)
}
