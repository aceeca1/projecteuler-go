package main

import (
	"fmt"
	"sort"
)

func init() { reg(47, solve47) }

func solve47() {
	var p = NewPrime16()
	var f = func(n int) bool {
		var a = p.Factorize32(n)
		sort.Ints(a)
		var h, b = a[0], a[:1]
		for i := 1; i < len(a); i++ {
			if a[i] != h {
				h = a[i]
				b = append(b, i)
				if len(b) > 4 {
					break
				}
			}
		}
		return len(b) == 4
	}
	var i = 2
	for h := 0; h < 4; i++ {
		if f(i) {
			h++
		} else {
			h = 0
		}
	}
	fmt.Println(i - 4)
}
