package main

import (
	"fmt"
	"math/big"
)

func init() { reg(55, solve55) }

func solve55() {
	var m = 0
	for i := int64(1); i < 10000; i++ {
		var v = big.NewInt(i)
		var isL = true
		for j := 1; j <= 50; j++ {
			var vs = v.String()
			if j > 1 && isPalin(vs) {
				isL = false
				break
			}
			var vsb = ByteSlice([]byte(vs))
			reverse(&vsb)
			var b, _ = new(big.Int).SetString(string(vsb), 10)
			v.Add(v, b)
		}
		if isL {
			m++
		}
	}
	fmt.Println(m)
}
