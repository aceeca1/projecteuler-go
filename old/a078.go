package main

import (
	"fmt"
)

func init() { reg(78, solve78) }

func solve78() {
	var f = func(n int) []int {
		var a = make([]int, n+1)
		a[0] = 1
		for i := 1; i <= n; i++ {
			for j := i; j <= n; j++ {
				a[j] = (a[j] + a[j-i]) % 1000000
			}
		}
		return a
	}
	var n, i = 1, 1
	for {
		var a = f(n)
		for ; i < len(a); i++ {
			if a[i] == 0 {
				fmt.Println(i)
				return
			}
		}
		n += n
	}
}
