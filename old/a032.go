package main

import (
	"fmt"
	"strconv"
)

func init() { reg(32, solve32) }

func solve32() {
	var a = map[int]bool{}
	var f = func(n1, n2 int) {
		for i := n1*10 - 1; i >= n1; i-- {
			var k = n2 * 10
			for j := n2; j < k; j++ {
				var n = i * j
				if n >= 10000 {
					continue
				}
				var u [10]int
				digitsForeach(strconv.Itoa(i), func(i int) { u[i]++ })
				digitsForeach(strconv.Itoa(j), func(i int) { u[i]++ })
				digitsForeach(strconv.Itoa(n), func(i int) { u[i]++ })
				var perm = u[0] == 0
				for i := 1; i < 10; i++ {
					if u[i] != 1 {
						perm = false
						break
					}
				}
				if perm {
					a[n] = true
				}
			}
		}
	}
	f(10, 100)
	f(1, 1000)
	var m = 0
	for i, _ := range a {
		m += i
	}
	fmt.Println(m)
}
