package main

import (
	"fmt"
)

func init() { reg(6, solve6) }

func solve6() {
	var m int64
	for i := int64(1); i <= 100; i++ {
		m += i
	}
	m *= m
	for i := int64(1); i <= 100; i++ {
		m -= i * i
	}
	fmt.Println(m)
}
