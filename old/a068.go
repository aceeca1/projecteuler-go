package main

import (
	"fmt"
)

func init() { reg(68, solve68) }

func solve68() {
	var a [10]int
	var s int
	var f func(int) bool
	f = func(i int) bool {
		if i >= 10 {
			for _, j := range [15]int{0, 1, 2, 3, 2, 4,
				5, 4, 6, 7, 6, 8, 9, 8, 1} {
				fmt.Print(a[j])
			}
			fmt.Println()
			return true
		}
		var uB, lB = 9, 1
		switch i {
		case 0:
			uB, lB = 6, 1
		case 3, 5, 7, 9:
			uB, lB = 9, a[0]
		}
		for ai := uB; ai >= lB; ai-- {
			a[i] = ai
			switch i {
			case 3, 5, 7, 9:
				if ai == lB {
					a[i] = 10
				}
			}
			if func() bool {
				for j := 0; j < i; j++ {
					if a[i] == a[j] {
						return false
					}
				}
				switch i {
				case 2:
					s = a[0] + a[1] + a[2]
				case 4, 6, 8:
					if s != a[i]+a[i-1]+a[i-2] {
						return false
					}
				case 9:
					if s != a[9]+a[8]+a[1] {
						return false
					}
				}
				return true
			}() && f(i+1) {
				return true
			}
		}
		return false
	}
	f(0)
}
