package main

import (
	"fmt"
	"math/big"
)

func init() { reg(2, solve2) }

func solve2() {
	var s int64
	for i := NewFib(); i.Get().Cmp(big.NewInt(4000000)) <= 0; i.Next() {
		if i.Get().Int64()&1 == 0 {
			s += i.Get().Int64()
		}
	}
	fmt.Println(s)
}
