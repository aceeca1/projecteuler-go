package main

import (
	"fmt"
	"strconv"
)

func init() { reg(74, solve74) }

func solve74() {
	var fac = [10]int{1}
	for i, p := 1, 1; i < 10; i++ {
		p = p * i
		fac[i] = p
	}
	var f = func(n int) (s int) {
		digitsForeach(strconv.Itoa(n), func(i int) { s += fac[i] })
		return
	}
	var ch = make([]int, fac[9]*7+1)
	var chain func(int, int) (bool, int)
	chain = func(n, m int) (c1 bool, c2 int) {
		switch {
		case ch[n] == 0:
			ch[n] = m
			c1, c2 = chain(f(n), m-1)
			if !c1 {
				c2++
			}
			c1 = c1 && ch[n] != 0
			ch[n] = c2
		case ch[n] < 0:
			c1, c2 = true, ch[n]-m
			ch[n] = 0
		default:
			c1, c2 = false, ch[n]
		}
		return
	}
	var a = 0
	for i := 1; i <= 1000000; i++ {
		var _, c = chain(i, -1)
		if c == 60 {
			a++
		}
	}
	fmt.Println(a)
}
