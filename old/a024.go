package main

import (
	"fmt"
)

func init() { reg(24, solve24) }

func solve24() {
	var a = append([]byte{}, "0123456789"...)
	var fac = [10]int{1, 1}
	for i := 2; i < 10; i++ {
		fac[i] = fac[i-1] * i
	}
	var b = []byte{}
	var k = 999999
	for len(a) > 0 {
		var i = k / fac[len(a)-1]
		k %= fac[len(a)-1]
		b = append(b, a[i])
		a = append(a[:i], a[i+1:]...)
	}
	fmt.Println(string(b))
}
