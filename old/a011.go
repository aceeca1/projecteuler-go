package main

import (
	"fmt"
	"strings"
)

func init() { reg(11, solve11) }

func solve11() {
	var in = strings.NewReader(a011Data)
	var a [20][20]int
	for i := 0; i < 20; i++ {
		for j := 0; j < 20; j++ {
			fmt.Fscanf(in, "%d", &a[i][j])
		}
	}
	var m = 0
	for i0 := 0; i0 < 20; i0++ {
		for j0 := 0; j0 < 20; j0++ {
			var k = [4][2]int{{1, 1}, {1, 0}, {1, -1}, {0, 1}}
			for ki := 0; ki < 4; ki++ {
				var i1, j1 = i0 + k[ki][0], j0 + k[ki][1]
				var i2, j2 = i1 + k[ki][0], j1 + k[ki][1]
				var i3, j3 = i2 + k[ki][0], j2 + k[ki][1]
				if i3 >= 20 || j3 >= 20 || j3 < 0 {
					continue
				}
				var b = a[i0][j0] * a[i1][j1] * a[i2][j2] * a[i3][j3]
				if b > m {
					m = b
				}
			}
		}
	}
	fmt.Println(m)
}
