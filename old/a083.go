package main

import (
	"fmt"
)

func init() { reg(83, solve83) }

func solve83() {
	var g = new(sPGraph)
	var b = [len(a081Data)][len(a081Data[0])]int{}
	for i := range b {
		for j := range b[i] {
			b[i][j] = g.addVertex()
			if j != 0 {
				g.addEdge(b[i][j-1], b[i][j], a081Data[i][j])
				g.addEdge(b[i][j], b[i][j-1], a081Data[i][j-1])
			}
			if i != 0 {
				g.addEdge(b[i-1][j], b[i][j], a081Data[i][j])
				g.addEdge(b[i][j], b[i-1][j], a081Data[i-1][j])
			}
		}
	}
	g.shortestPath(0)
	fmt.Println((*g)[len(*g)-1].d + a081Data[0][0])
}
