package main

import (
	"fmt"
	"math/big"
	"strings"
)

func init() { reg(13, solve13) }

func solve13() {
	var in = strings.NewReader(a013Data)
	var sum = big.NewInt(0)
	for i := 0; i < 100; i++ {
		var n = new(big.Int)
		fmt.Fscan(in, n)
		sum.Add(sum, n)
	}
	fmt.Println(sum.String()[:10])
}
