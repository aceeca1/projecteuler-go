package main

import (
	"math/big"
)

type Fib struct{ a0, a1 *big.Int }

func NewFib() Fib { return Fib{big.NewInt(0), big.NewInt(1)} }

func (i *Fib) Next() { i.a0, i.a1 = i.a1, i.a0.Add(i.a0, i.a1) }

func (i *Fib) Get() *big.Int { return i.a0 }
