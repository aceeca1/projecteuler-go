package main

import (
	"fmt"
	"math"
)

func init() { reg(85, solve85) }

func solve85() {
	var a, b, m, mI = 1, 2000, math.MaxInt32, 0
	for b > 0 {
		var c = (a * (a + 1) * b * (b + 1) >> 2) - 2000000
		if c > 0 {
			if c < m {
				m, mI = c, a*b
			}
			b--
		} else {
			c = -c
			if c < m {
				m, mI = c, a*b
			}
			a++
		}
	}
	fmt.Println(mI)
}
