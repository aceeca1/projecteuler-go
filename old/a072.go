package main

import (
	"fmt"
)

func init() { reg(72, solve72) }

func solve72() {
	var p = NewPrime16().phi(1000000)
	var s = int64(0)
	for i := 2; i < len(p); i++ {
		s += int64(p[i])
	}
	fmt.Println(s)
}
