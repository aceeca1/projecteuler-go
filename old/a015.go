package main

import (
	"fmt"
)

func init() { reg(15, solve15) }

func solve15() {
	fmt.Printf("%.0f\n", choose(40, 20))
}
