package main

import (
	"fmt"
)

func init() { reg(76, solve76) }

func solve76() {
	const n = 100
	var a = [n + 1]int{1}
	for i := 1; i <= n; i++ {
		for j := i; j <= n; j++ {
			a[j] += a[j-i]
		}
	}
	fmt.Println(a[n] - 1)
}
