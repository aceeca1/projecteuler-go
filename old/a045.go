package main

import (
	"fmt"
)

func init() { reg(45, solve45) }

func solve45() {
	for i := int64(144); ; i++ {
		var n = polyNum(6, i)
		if isPolyNum(5, n) && isPolyNum(3, n) {
			fmt.Println(n)
			return
		}
	}
}
