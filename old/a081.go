package main

import (
	"fmt"
	"math"
)

func init() { reg(81, solve81) }

func solve81() {
	var b = [len(a081Data[0])]int{}
	for i := 1; i < len(b); i++ {
		b[i] = math.MaxInt32
	}
	for _, ai := range a081Data {
		b[0] += ai[0]
		for j := 1; j < len(ai); j++ {
			if b[j-1] < b[j] {
				b[j] = b[j-1]
			}
			b[j] += ai[j]
		}
	}
	fmt.Println(b[len(b)-1])
}
