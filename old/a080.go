package main

import (
	"fmt"
	"math"
	"math/big"
)

func init() { reg(80, solve80) }

func solve80() {
	var gg2, s = big.NewInt(10), 0
	gg2.Exp(gg2, big.NewInt(200), nil)
	for i := 1; i <= 100; i++ {
		var r = int(math.Sqrt(float64(i)))
		if r*r != i {
			var iB = big.NewInt(int64(i))
			digitsForeach(
				bigIntSqrt(iB.Mul(iB, gg2)).String()[:100],
				func(i int) { s += i })
		}
	}
	fmt.Println(s)
}
