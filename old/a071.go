package main

import (
	"fmt"
)

func init() { reg(71, solve71) }

func solve71() {
	var m, mI = 0.0, 0
	for i := 1000000 * 3 / 7; i > 0; i-- {
		if u := float64(i) / (float64(i)*(7.0/3.0) + 1.0); u > m {
			m, mI = u, i
		}
	}
	fmt.Println(mI)
}
