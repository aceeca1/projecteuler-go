package main

import (
	"fmt"
	"strconv"
)

func init() { reg(35, solve35) }

func solve35() {
	var f = func(s string, i int) string {
		i = i % len(s)
		return s[i:] + s[:i]
	}
	var m = map[string]bool{}
	for i := NewPrime16(); i.Get() < 1000000; i.Next() {
		m[strconv.Itoa(i.Get())] = true
	}
	var g = func(k int) {
		for i := range m {
			var _, ok = m[f(i, k)]
			if !ok {
				delete(m, i)
			}
		}
	}
	g(4)
	g(2)
	g(1)
	fmt.Println(len(m))
}
