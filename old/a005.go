package main

import (
	"fmt"
	"math/big"
)

func init() { reg(5, solve5) }

func solve5() {
	var m = big.NewInt(1)
	for i := int64(1); i <= 20; i++ {
		var b = big.NewInt(i)
		m.Mul(m, b.Div(b, new(big.Int).GCD(nil, nil, b, m)))
	}
	fmt.Println(m)
}
