package main

import (
	"fmt"
)

func init() { reg(18, solve18) }

func solve18() {
	fmt.Println(maxPathSum(15, a018Data))
}
