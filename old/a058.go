package main

import (
	"fmt"
)

func init() { reg(58, solve58) }

func solve58() {
	var p = NewPrime16()
	var m1, m2, k = 1, 0, 1
	for i := 2; ; i += 2 {
		for j := 0; j < 4; j++ {
			m1 += i
			if p.Prime32(m1) {
				m2 = m2 + 1
			}
			k++
			if j == 3 && float64(m2) < 0.1*float64(k) {
				fmt.Println(i + 1)
				return
			}
		}
	}
}
