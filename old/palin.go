package main

func isPalin(s string) bool {
	for i, j := 0, len(s)-1; i < j; {
		if s[i] != s[j] {
			return false
		}
		i++
		j--
	}
	return true
}

type Palin struct {
	z int
	a []int
}

func NewPalin() *Palin { return &Palin{1, []int{0}} }

func (m *Palin) Next() {
	if func() bool {
		for _, i := range m.a {
			if i != 9 {
				return false
			}
		}
		return true
	}() {
		for i := range m.a {
			m.a[i] = 0
		}
		if m.z&1 == 0 {
			m.a = append(m.a, 1)
		} else {
			m.a[len(m.a)-1] = 1
		}
		m.z++
	} else {
		var i = 0
		for m.a[i] == 9 {
			m.a[i] = 0
			i++
		}
		m.a[i]++
	}
}

func (m *Palin) Get() (r int) {
	for i := len(m.a) - 1; i >= 0; i-- {
		r = r*10 + m.a[i]
	}
	for i := m.z & 1; i < len(m.a); i++ {
		r = r*10 + m.a[i]
	}
	return
}
