package main

import (
	"fmt"
	"math"
	"sort"
	"strconv"
)

func init() { reg(62, solve62) }

func solve62() {
	var z = 1
	var m = map[string][2]int64{}
	for i := int64(1); ; i++ {
		var i3 = i * i * i
		var b = []byte(strconv.FormatInt(i3, 10))
		sort.Sort(ByteSlice(b))
		var s = string(b)
		if len(s) > z {
			var a = int64(math.MaxInt64)
			for _, j := range m {
				if j[0] >= 5 && j[1] < a {
					a = j[1]
				}
			}
			if a != int64(math.MaxInt64) {
				fmt.Println(a)
				return
			}
			m = map[string][2]int64{}
			z = len(s)
		} else {
			if ms, ok := m[s]; ok {
				m[s] = [2]int64{ms[0] + 1, ms[1]}
			} else {
				m[s] = [2]int64{1, i * i * i}
			}
		}
	}
}
