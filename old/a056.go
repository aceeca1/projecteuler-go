package main

import (
	"fmt"
	"math/big"
)

func init() { reg(56, solve56) }

func solve56() {
	var m = 0
	for i := int64(2); i < 100; i++ {
		var iB = big.NewInt(i)
		var k = new(big.Int).Set(iB)
		for j := 2; j < 100; j++ {
			k.Mul(k, iB)
			var s = 0
			digitsForeach(k.String(), func(i int) { s += i })
			if s > m {
				m = s
			}
		}
	}
	fmt.Println(m)
}
