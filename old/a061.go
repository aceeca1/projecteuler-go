package main

import (
	"fmt"
)

func init() { reg(61, solve61) }

func solve61() {
	var z = [6][100][]int{}
	for i := 3; i <= 8; i++ {
		for j := 10; j < 100; j++ {
			for k := 10; k < 100; k++ {
				if isPolyNum(int64(i), int64(j*100+k)) {
					z[i-3][j] = append(z[i-3][j], j*100+k)
				}
			}
		}
	}
	var p = [...]int{0, 1, 2, 3, 4}
	for i := true; i; i = nextPerm(p[:]) {
		for u5, v5 := range z[5] {
			for _, i5 := range v5 {
				for _, i0 := range z[p[0]][i5%100] {
					for _, i1 := range z[p[1]][i0%100] {
						for _, i2 := range z[p[2]][i1%100] {
							for _, i3 := range z[p[3]][i2%100] {
								for _, i4 := range z[p[4]][i3%100] {
									if i4%100 == u5 {
										fmt.Println(i0 + i1 + i2 + i3 + i4 + i5)
										return
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
