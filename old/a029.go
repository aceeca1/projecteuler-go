package main

import (
	"fmt"
	"math/big"
)

func init() { reg(29, solve29) }

func solve29() {
	var m = map[string]bool{}
	for i := int64(2); i <= 100; i++ {
		var k, n = big.NewInt(i), big.NewInt(i)
		for j := 2; j <= 100; j++ {
			n.Mul(n, k)
			m[n.String()] = true
		}
	}
	fmt.Println(len(m))
}
