package main

import (
	"fmt"
)

func init() { reg(43, solve43) }

func solve43() {
	var n = [...]int{1, 0, 2, 3, 4, 5, 6, 7, 8, 9}
	var s = 0
	var f = func(a []int) (r int) {
		for _, i := range a {
			r = r*10 + i
		}
		return
	}
	for nextPerm(n[:]) {
		if n[3]&1 != 0 || n[5]%5 != 0 || f(n[7:10])%17 != 0 ||
			f(n[6:9])%13 != 0 || f(n[5:8])%11 != 0 || f(n[4:7])%7 != 0 ||
			f(n[2:5])%3 != 0 {
			continue
		}
		s += f(n[:])
	}
	fmt.Println(s)
}
