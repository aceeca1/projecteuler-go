package main

import (
	"fmt"
)

func init() { reg(42, solve42) }

func solve42() {
	var n = 0
	for _, i := range a042Data {
		var m = int64(0)
		for _, j := range i {
			m += int64(j - 64)
		}
		if isPolyNum(3, m) {
			n++
		}
	}
	fmt.Println(n)
}
