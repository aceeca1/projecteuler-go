package main

import (
	"fmt"
	"math/big"
)

func init() { reg(57, solve57) }

func solve57() {
	var m, a, b = 0, big.NewInt(3), big.NewInt(2)
	for i := 1000; i != 0; i-- {
		if len(a.String()) > len(b.String()) {
			m++
		}
		var c = b
		b = a.Add(a, b)
		a = c.Add(c, b)
	}
	fmt.Println(m)
}
