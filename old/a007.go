package main

import (
	"fmt"
)

func init() { reg(7, solve7) }

func solve7() {
	var p = NewPrime16()
	for i := 1; i <= 10000; i++ {
		p.Next()
	}
	fmt.Println(p.i)
}
