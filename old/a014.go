package main

import (
	"fmt"
)

func init() { reg(14, solve14) }

func solve14() {
	var a = [1000000]bool{}
	var m, mI = 0, 0
	for i := 999999; i > 0; i-- {
		if a[i] {
			continue
		}
		var z, k = 1, i
		for k != 1 {
			if k&1 == 0 {
				k = k >> 1
			} else {
				k = k*3 + 1
			}
			if k < 1000000 {
				a[k] = true
			}
			z += 1
		}
		if z > m {
			m, mI = z, i
		}
	}
	fmt.Println(mI)
}
