package main

import (
	"fmt"
)

func init() { reg(48, solve48) }

func solve48() {
	var sum = int64(0)
	for i := 1; i <= 1000; i++ {
		var m = int64(i)
		for j := 2; j <= i; j++ {
			m = m * int64(i) % 10000000000
		}
		sum += m
	}
	fmt.Println(sum % 10000000000)
}
