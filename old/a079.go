package main

import (
	"container/heap"
	"fmt"
)

func init() { reg(79, solve79) }

type a079Node struct {
	s []byte
	v []string
	p int
}
type a079Heap struct{ AnyS }

func newA079Node(s []byte, v []string) *a079Node {
	var p = 0
	for _, i := range v {
		if len(i) > p {
			p = len(i)
		}
	}
	return &a079Node{s, v, p + len(s)}
}
func (n *a079Node) succeed() bool { return len(n.v) == 0 }

func (m *a079Heap) Less(i, j int) bool {
	var mi, mj = (*m.AnyS[i]).(a079Node), (*m.AnyS[j]).(a079Node)
	return mi.p < mj.p
}

func solve79() {
	var q = new(a079Heap)
	heap.Push(q, *newA079Node([]byte{}, a079Data[:]))
	for {
		var qH = heapPop(q).(a079Node)
		var m = map[byte]bool{}
		for _, i := range qH.v {
			var i0 = i[0]
			if !m[i0] {
				m[i0] = true
				var vN = []string{}
				for _, j := range qH.v {
					if j[0] == i0 {
						j = j[1:]
					}
					if len(j) != 0 {
						vN = append(vN, j)
					}
				}
				var qN = newA079Node(append(append([]byte{}, qH.s...), i0), vN)
				if qN.succeed() {
					fmt.Println(string(qN.s))
					return
				}
				heap.Push(q, *qN)
			}
		}
	}
}
