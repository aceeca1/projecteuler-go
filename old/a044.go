package main

import (
	"fmt"
)

func init() { reg(44, solve44) }

func solve44() {
	var pr = NewPrime16()
	for a := int64(1); ; a++ {
		var ap = polyNum(5, a)
		var pq, k = ap + ap, true
		for i := pr.divisors(int(pq)); k; k = i.Next() {
			var p = int64(i.Get())
			var q = pq / p
			var b = (p + 1 - 3*q) / 6
			if b <= 0 || p != 3*(b+b+q)-1 {
				continue
			}
			var bp = polyNum(5, b)
			var cp = ap + bp
			var dp = bp + cp
			if !isPolyNum(5, dp) {
				continue
			}
			fmt.Println(ap)
			return
		}
	}
}
