package main

import (
	"fmt"
)

func init() { reg(84, solve84) }

type a084Matrix [40][40]float64

func a084Sqr(a a084Matrix) (r a084Matrix) {
	for i := 0; i < 40; i++ {
		for j := 0; j < 40; j++ {
			for k := 0; k < 40; k++ {
				r[i][k] += a[i][j] * a[j][k]
			}
		}
	}
	return r
}

func solve84() {
	var threeDoubleJail = 3.0 / (1 << 10)
	var a = [40]float64{}
	for i1 := 1; i1 <= 4; i1++ {
		for i2 := 1; i2 <= 4; i2++ {
			a[i1+i2] += 1.0 / 16
		}
	}
	for i := 2; i <= 8; i += 2 {
		a[i] -= threeDoubleJail
	}
	var b = a084Matrix{}
	for i1 := 0; i1 < 40; i1++ {
		for i2 := 0; i2 < 40; i2++ {
			var i = i2 - i1
			if i < 0 {
				i += 40
			}
			if i2 == 10 {
				b[i1][i2] = a[i] + threeDoubleJail*4.0
			} else {
				b[i1][i2] = a[i]
			}
		}
	}
	for k, v := range map[int][]int{
		30: []int{
			10, 10, 10, 10, 10, 10, 10, 10,
			10, 10, 10, 10, 10, 10, 10, 10},
		7:  []int{0, 10, 11, 24, 39, 5, 15, 15, 12, 4},
		22: []int{0, 10, 11, 24, 39, 5, 25, 25, 28, 19},
		36: []int{0, 10, 11, 24, 39, 5, 5, 5, 12, 33},
		2:  []int{0, 10},
		17: []int{0, 10},
		33: []int{0, 10},
	} {
		for i := 0; i < 40; i++ {
			var c = b[i][k] * (1.0 / 16)
			b[i][k] = c * float64(16-len(v))
			for _, j := range v {
				b[i][j] += c
			}
		}
	}
	for i := 1; i <= 30; i++ {
		b = a084Sqr(b)
	}
	var u = [40]int{}
	for i := 0; i < 40; i++ {
		u[i] = i
	}
	sortF(40,
		func(i, j int) bool { return b[0][u[i]] > b[0][u[j]] },
		func(i, j int) { u[i], u[j] = u[j], u[i] })
	fmt.Print(u[0])
	fmt.Print(u[1])
	fmt.Println(u[2])
}
