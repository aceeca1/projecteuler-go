package main

import (
	"fmt"
)

func init() { reg(21, solve21) }

func solve21() {
	var sum = 0
	var p = NewPrime16()
	for i := 2; i < 10000; i++ {
		var k = p.divisor(i, toSum) - i
		if k != i && p.divisor(k, toSum)-k == i {
			sum += i
		}
	}
	fmt.Println(sum)
}
