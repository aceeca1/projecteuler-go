package main

import (
	"fmt"
)

func init() { reg(25, solve25) }

func solve25() {
	var c = 0
	for i := NewFib(); len(i.Get().String()) < 1000; i.Next() {
		c++
	}
	fmt.Println(c)
}
