package main

import (
	"fmt"
	"math/big"
)

func init() { reg(3, solve3) }

func solve3() {
	m := big.NewInt(0)
	for _, i := range Factorize(big.NewInt(600851475143)) {
		if i.Cmp(m) > 0 {
			m = i
		}
	}
	fmt.Println(m)
}
