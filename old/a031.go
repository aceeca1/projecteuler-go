package main

import (
	"fmt"
)

func init() { reg(31, solve31) }

func solve31() {
	var v = [201]int{1}
	for _, i := range [...]int{1, 2, 5, 10, 20, 50, 100, 200} {
		for j := i; j <= 200; j++ {
			v[j] += v[j-i]
		}
	}
	fmt.Println(v[200])
}
