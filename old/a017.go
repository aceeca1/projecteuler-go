package main

import (
	"fmt"
)

func init() { reg(17, solve17) }

func a017Pron(n int) int {
	switch n {
	case 1:
		return len("one")
	case 2:
		return len("two")
	case 3:
		return len("three")
	case 4:
		return len("four")
	case 5:
		return len("five")
	case 6:
		return len("six")
	case 7:
		return len("seven")
	case 8:
		return len("eight")
	case 9:
		return len("nine")
	case 10:
		return len("ten")
	case 11:
		return len("eleven")
	case 12:
		return len("twelve")
	case 13:
		return len("thirteen")
	case 14:
		return len("fourteen")
	case 15:
		return len("fifteen")
	case 16:
		return len("sixteen")
	case 17:
		return len("seventeen")
	case 18:
		return len("eighteen")
	case 19:
		return len("nineteen")
	case 20:
		return len("twenty")
	case 30:
		return len("thirty")
	case 40:
		return len("forty")
	case 50:
		return len("fifty")
	case 60:
		return len("sixty")
	case 70:
		return len("seventy")
	case 80:
		return len("eighty")
	case 90:
		return len("ninety")
	case 1000:
		return len("one") + len("thousand")
	}
	switch {
	case n < 100:
		return a017Pron(n/10*10) + a017Pron(n%10)
	case n%100 == 0:
		return len("hundred") + a017Pron(n/100)
	default:
		return a017Pron(n/100*100) + len("and") + a017Pron(n%100)
	}
}

func solve17() {
	var sum = 0
	for i := 1; i <= 1000; i++ {
		sum += a017Pron(i)
	}
	fmt.Println(sum)
}
