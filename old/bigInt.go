package main

import (
	"math/big"
)

func bigIntSqrt(n *big.Int) *big.Int {
	var one = big.NewInt(1)
	var r = new(big.Int).Lsh(one, uint(n.BitLen()>>1))
	for {
		var a = new(big.Int).Div(n, r)
		a.Add(a, r).Add(a, one).Rsh(a, 1)
		if a.Cmp(r) == 0 {
			break
		}
		r = a
	}
	if new(big.Int).Mul(r, r).Cmp(n) > 0 {
		r.Sub(r, one)
	}
	return r
}
