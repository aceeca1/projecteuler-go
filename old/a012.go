package main

import (
	"fmt"
)

func init() { reg(12, solve12) }

func solve12() {
	var p = NewPrime16()
	for i := 2; ; i++ {
		var a = i * (i + 1) >> 1
		if p.divisor(a, toNum) > 500 {
			fmt.Println(a)
			return
		}
	}
}
