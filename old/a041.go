package main

import (
	"fmt"
)

func init() { reg(41, solve41) }

func solve41() {
	var p = NewPrime16()
	var a = []int{1, 2, 3, 4, 5, 6, 7}
	for nextPerm(a) {
		var n = 0
		for _, v := range a {
			n = n*10 + 8 - v
		}
		if p.Prime32(n) {
			fmt.Println(n)
			return
		}
	}
}
