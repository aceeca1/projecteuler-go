package main

import (
	"fmt"
)

func init() { reg(59, solve59) }

func solve59() {
	var c, k = [3][]byte{}, 0
	for _, i := range a059Data {
		c[k] = append(c[k], i)
		k = (k + 1) % 3
	}
	var sum = 0
	for _, i := range c {
		var h = [256]int{}
		for _, j := range i {
			h[j]++
		}
		var m, mj = 0, 0
		for j, v := range h {
			if v > m {
				m = v
				mj = j
			}
		}
		var key = mj ^ ' '
		for _, j := range i {
			sum += int(j) ^ key
		}
	}
	fmt.Println(sum)
}
