package main

import (
	"fmt"
	"sort"
	"strconv"
)

func init() { reg(52, solve52) }

func solve52() {
	for i := 1; ; i++ {
		var f = func(n int) string {
			var a = []byte(strconv.Itoa(i * n))
			sort.Sort(ByteSlice(a))
			return string(a)
		}
		var f2 = f(2)
		if f(3) == f2 && f(4) == f2 && f(5) == f(2) && f(6) == f2 {
			fmt.Println(i)
			return
		}
	}
}
