package main

import (
	"fmt"
	"sort"
	"strconv"
)

func init() { reg(38, solve38) }

func solve38() {
	var m = 0
	for i := 1; i < 10000; i++ {
		var b = []byte(strconv.Itoa(i))
		var i10 = i * 10
		for j := i + i; j < i10; j += i {
			b = append(b, strconv.Itoa(j)...)
			if len(b) > 9 {
				break
			}
			var n, _ = strconv.Atoi(string(b))
			sort.Sort(ByteSlice(b))
			if string(b) == "123456789" && n > m {
				m = n
			}
		}
	}
	fmt.Println(m)
}
