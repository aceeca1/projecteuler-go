package main

import (
	"fmt"
)

func init() { reg(9, solve9) }

func solve9() {
	for m := 1; m <= 500; m++ {
		if 500%m != 0 {
			continue
		}
		var kp = 500 / m
		for p := m + 1; p < m+m; p++ {
			if kp%p != 0 {
				continue
			}
			var k = kp / p
			var n = p - m
			var m2 = m * m
			var n2 = n * n
			var a = k * (m2 - n2)
			var b = k * m * n << 1
			var c = k * (m2 + n2)
			fmt.Println(a * b * c)
			return
		}
	}
}
