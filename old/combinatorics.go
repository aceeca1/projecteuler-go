package main

func choose(n, k int) float64 {
	var r = 1.0
	for i := 1; i <= k; i++ {
		r = r * (float64(n-i+1) / float64(i))
	}
	return r
}
