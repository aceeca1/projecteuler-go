package main

import (
	"fmt"
)

func init() { reg(26, solve26) }

func solve26() {
	var m, mI = 0, 0
	for i := 1; i < 1000; i++ {
		var n = findCycle(IntEq(1), func(n *Eq) {
			*n = (*n).(IntEq) * 10 % IntEq(i)
		})
		if n > m {
			m, mI = n, i
		}
	}
	fmt.Println(mI)
}
