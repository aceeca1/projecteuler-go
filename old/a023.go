package main

import (
	"fmt"
)

func init() { reg(23, solve23) }

func solve23() {
	var a [28124]bool
	var b []int
	var p = NewPrime16()
	for i := 2; i < 28124; i++ {
		if p.divisor(i, toSum)-i > i {
			a[i] = true
			b = append(b, i)
		}
	}
	var sum = 0
	for i := 1; i < 28124; i++ {
		var able = false
		for _, j := range b {
			if i-j < 0 {
				break
			}
			if a[i-j] {
				able = true
				break
			}
		}
		if !able {
			sum += i
		}
	}
	fmt.Println(sum)
}
