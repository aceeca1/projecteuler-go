package main

import (
	"fmt"
)

func init() { reg(37, solve37) }

func solve37() {
	var p = NewPrime16()
	var f func(int) int
	f = func(a int) (r int) {
		for _, i := range []int{1, 3, 7, 9} {
			var ai = a*10 + i
			if !p.Prime32(ai) {
				continue
			}
			r += f(ai)
		}
		var i = 10
		for i < a {
			if !p.Prime32(a % i) {
				return
			}
			i = i * 10
		}
		return r + a
	}
	var r = 0 - 2 - 3 - 5 - 7
	for _, i := range []int{2, 3, 5, 7} {
		r += f(i)
	}
	fmt.Println(r)
}
