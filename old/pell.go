package main

import (
	"math"
	"math/big"
)

func pell(n int) (*big.Int, *big.Int) {
	var s = int(math.Sqrt(float64(n)))
	if s*s == n {
		return nil, nil
	}
	var one = big.NewInt(1)
	var isSolution = func(p, q *big.Int) bool {
		var nq2 = big.NewInt(int64(n))
		nq2.Mul(nq2, q).Mul(nq2, q)
		var p2 = new(big.Int).Mul(p, p)
		return p2.Sub(p2, nq2).Cmp(one) == 0
	}
	var p0, q0 = big.NewInt(int64(s)), one
	if isSolution(p0, q0) {
		return p0, q0
	}
	var v1 = n - s*s
	var a1 = (s + s) / v1
	var q1 = big.NewInt(int64(a1))
	var u1, p1 = s, new(big.Int).Mul(p0, q1)
	p1.Add(p1, one)
	if isSolution(p1, q1) {
		return p1, q1
	}
	for {
		var u2 = a1*v1 - u1
		var v2 = (n - u2*u2) / v1
		var a2 = (s + u2) / v2
		var p2 = big.NewInt(int64(a2))
		p2.Mul(p2, p1).Add(p2, p0)
		var q2 = big.NewInt(int64(a2))
		q2.Mul(q2, q1).Add(q2, q0)
		if isSolution(p2, q2) {
			return p2, q2
		}
		p0, q0, u1, v1, a1, p1, q1 = p1, q1, u2, v2, a2, p2, q2
	}
}
