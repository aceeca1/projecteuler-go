package main

import (
	"fmt"
	"math"
)

func init() { reg(64, solve64) }

func solve64() {
	var a = 0
	for i := 1; i <= 10000; i++ {
		var sI = math.Sqrt(float64(i))
		if int(sI)*int(sI) == i {
			continue
		}
		if findCycle(Int3Eq([3]int{0, 0, i}), func(k *Eq) {
			var b = (*k).(Int3Eq)
			b[2] = (i - b[1]*b[1]) / b[2]
			b[0] = int((sI - float64(b[1])) / float64(b[2]))
			b[1] = -b[0]*b[2] - b[1]
			*k = b
		})&1 != 0 {
			a = a + 1
		}
	}
	fmt.Println(a)
}
