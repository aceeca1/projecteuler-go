package main

import (
	"sort"
)

const PZ = 65536
const PP = 65521

type Prime16 struct {
	p *[PZ]uint16
	i int
}

func NewPrime16() *Prime16 {
	p := new([PZ]uint16)
	var u = 0
	for i := 2; i < PZ; i++ {
		var v = int(p[i])
		if v == 0 {
			v = i
			p[u] = uint16(i)
			u = i
		}
		for w := 2; i*w < PZ; w = int(p[w]) {
			p[i*w] = uint16(w)
			if w >= v {
				break
			}
		}
	}
	p[u] = PZ - 1
	return &Prime16{p, 2}
}

func (m *Prime16) Iter() *Prime16 { return &Prime16{m.p, 2} }

func (m *Prime16) Prime16(n int) bool { return int(m.p[n]) > n }

func (m *Prime16) Prime32(n int) bool {
	if n < PZ {
		return m.Prime16(n)
	}
	for i := m.Iter(); i.Get()*i.Get() <= n; i.Next() {
		if n%i.Get() == 0 {
			return false
		}
	}
	return true
}

func (m *Prime16) Next() {
	if m.i < PP {
		m.i = int(m.p[m.i])
	} else {
		m.i += 2
		for !m.Prime32(m.i) {
			m.i += 2
		}
	}
}

func (m *Prime16) Get() int { return m.i }

func (m *Prime16) minFactor16(n int) int { return int(m.p[n]) }

func (m *Prime16) Factorize16(n int) (r []int) {
	if n == 1 {
		return
	}
	for !m.Prime16(n) {
		var mF = m.minFactor16(n)
		r = append(r, mF)
		n = n / mF
	}
	return append(r, n)
}

func (m *Prime16) minFactor32(n int) int {
	for i := m.Iter(); i.Get()*i.Get() <= n; i.Next() {
		if n%i.Get() == 0 {
			return i.Get()
		}
	}
	return n
}

func (m *Prime16) Factorize32(n int) (r []int) {
	for i := m.Iter(); n >= PZ && i.Get()*i.Get() <= n; i.Next() {
		for n%i.Get() == 0 {
			r = append(r, i.Get())
			n = n / i.Get()
		}
	}
	if n < PZ {
		return append(r, m.Factorize16(n)...)
	}
	return append(r, n)
}

const (
	toNum = iota
	toSum = iota
)

func (m *Prime16) divisor(n int, fun int) int {
	if n <= 1 {
		return 1
	}
	var b = m.Factorize32(n)
	sort.Ints(b)
	b = append(b, b[len(b)-1]+1)
	var p = b[0]
	var c = 2
	if fun == toSum {
		c = b[0] + 1
	}
	var e = 1
	for _, i := range b[1:] {
		if p == i {
			if fun == toSum {
				c *= p
			}
			c++
		} else {
			e *= c
			p = i
			c = 2
			if fun == toSum {
				c = p + 1
			}
		}
	}
	return e
}

type Divisors struct {
	k, v, a []int
	u       int
}

func (m *Prime16) divisors(n int) (r Divisors) {
	var b = m.Factorize32(n)
	var h = map[int]int{}
	for _, i := range b {
		h[i]++
	}
	for i, j := range h {
		r.k = append(r.k, i)
		r.v = append(r.v, j)
		r.a = append(r.a, 0)
	}
	r.u = 1
	return
}

func (m *Divisors) Get() int {
	return m.u
}

func (m *Divisors) Next() bool {
	var i = 0
	for {
		if i >= len(m.a) {
			return false
		}
		if m.a[i] != m.v[i] {
			break
		}
		for m.a[i] > 0 {
			m.u /= m.k[i]
			m.a[i]--
		}
		i++
	}
	m.a[i]++
	m.u *= m.k[i]
	return true
}

func (m *Prime16) phi(n int) []int {
	var r = make([]int, n+1)
	r[1] = 1
	for i := 2; i <= n; i++ {
		var a = m.minFactor32(i)
		var b = i / a
		var c = m.minFactor32(b)
		if a == c {
			r[i] = r[b] * a
		} else {
			r[i] = r[b] * (a - 1)
		}
	}
	return r
}
