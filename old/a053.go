package main

import (
	"fmt"
)

func init() { reg(53, solve53) }

func solve53() {
	var m = 0
	for i := 0; i <= 100; i++ {
		for j := 0; j <= i; j++ {
			if choose(i, j) > 1e6 {
				m++
			}
		}
	}
	fmt.Println(m)
}
