package main

import (
	"fmt"
	"os"
)

var solve []func()

func reg(i int, f func()) {
	for i >= len(solve) {
		solve = append(solve, nil)
	}
	solve[i] = f
}

func main() {
	for _, s := range os.Args[1:] {
		var i int
		fmt.Sscan(s, &i)
		if i == -1 {
			return
		}
		if i <= 0 || i >= len(solve) {
			continue
		}
		solve[i]()
	}
	for {
		var i int
		fmt.Scan(&i)
		if i == -1 {
			return
		}
		if i <= 0 || i >= len(solve) {
			continue
		}
		solve[i]()
	}
}
